import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { Sentry, SentrySeverity } from 'react-native-sentry';

Sentry.config('https://0640fa7ffafe4639a15120a003996952@sentry.io/1277286').install();

// // set a custom message
// Sentry.captureMessage('TEST message', {
// 	level: SentrySeverity.Warning
// }); // Default SentrySeverity.Error
//
// // capture an exception
// Sentry.captureException(new Error('Oops!'), {
// 	logger: 'my.module'
// });

export default class App extends React.Component {
	componentDidMount() {
		 Sentry.nativeCrash();
	}

	render() {
		return (
			<View style={styles.container}>
				<Text>Open up App.js to start working on your app!</Text>
				<Text>Changes you make will automatically reload.</Text>
				<Text>Shake your phone to open the developer menu.</Text>
				<ImageBackground />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center'
	}
});
